﻿ VS Package 관리자 명령
    add-migration "migration-name"
    update-database

    Install-Package Blazorise.Bootstrap
    Install-Package Blazorise.Icons.FontAwesome
    Install-Package Blazorise.DataGrid


Dotnet Tool 명령
    dotnet ef database update
    dotnet ef migrations remove
    dotnet ef migrations add "add ef_item table"

    dotnet ef dbcontext scaffold "{연결문자열}" Microsoft.EntityFrameworkCore.SqlServer -o Models -t Blog -t Post --context-dir Context -c BlogContext --context-namespace New.Namespace


Dotnet Tool 설치
    dotnet tool install --global dotnet-ef
    dotnet tool update --global dotnet-ef
    dotnet add package Microsoft.EntityFrameworkCore.Design
    dotnet ef   //실행환경확인


 테이블과 동일한 구조의 파일이 Data폴더에 존재해야 함
    I{테이블이름}Repostory.cs        //해당 테이블에서 제공할 기능의 정의    
    {테이블이름}Respository.cs       //인터페이스에서 정의된 기능의 구현
    {테이블이름}.cs                  //테이블구조와 1:1로 대응하는 데이터

신규테이블을 추가하면 ApplicationDbContext 파일에 DbSet을 정의한다
신규테이블을 추가하면 Startup.cs 에 서비스를 등록해야 한다