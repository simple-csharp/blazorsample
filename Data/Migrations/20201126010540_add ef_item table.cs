﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BlazorSample.Data.Migrations
{
    public partial class addef_itemtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EF_Items",
                columns: table => new
                {
                    idx = table.Column<Guid>(nullable: false),
                    itemName = table.Column<string>(maxLength: 50, nullable: true),
                    itemModel = table.Column<string>(maxLength: 50, nullable: true),
                    itemCode = table.Column<string>(maxLength: 10, nullable: true),
                    remark = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EF_Items", x => x.idx);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EF_Items");
        }
    }
}
