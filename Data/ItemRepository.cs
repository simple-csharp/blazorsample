﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSample.Data
{
    public class ItemRepository : IItemRepository
    {
        private readonly ApplicationDbContext _context;
        public ItemRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        async public Task<Item> Add(Item item)
        {
            this._context.EF_Items.Add(item);
            await this._context.SaveChangesAsync();
            return item;
        }

        async public Task<List<Item>> Items()
        {
            return await this._context.EF_Items.ToListAsync();
        }

        async public void Remove(Item item)
        {
            this._context.Remove(item);
            await this._context.SaveChangesAsync();
        }

        async public void Update(Item item)
        {
            this._context.Update(item);
            await this._context.SaveChangesAsync();
        }
    }
}
