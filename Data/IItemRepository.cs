﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSample.Data
{
    interface IItemRepository
    {
        Task<Item> Add(Item item);
        Task<List<Item>> Items();
        void Remove(Item item);
        void Update(Item item);
    }
}
