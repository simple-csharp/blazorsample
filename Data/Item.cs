﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BlazorSample.Data
{
    public class Item
    {
        [Required, Key]
        public Guid idx { get; set; }
        [MaxLength(50)]
        public string itemName { get; set; }
        [MaxLength(50)]
        public string itemModel { get; set; }
        [MaxLength(10)]
        public string itemCode { get; set; }
        [MaxLength(255)]
        public string remark { get; set; }
    }
}
